from django.shortcuts import render, get_object_or_404
from listings.models import Listing
from django.core.paginator import Paginator
from listings.choices import state_choices, price_choices, bedroom_choices

# Create your views here.

def index(request):
    listngs=Listing.objects.all()
    paginator=Paginator(listngs,2)

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context={'listings':page_obj}
    return render(request,'listings/listings.html', context)

def listing(request, listing_id):

    ##listing=Listing.objects.get(id=listing_id)
    listing=get_object_or_404(Listing, pk=listing_id)

    #return render(request,'listings/listing.html', {'listing':lintng})
    context={'listing':listing}
    return render(request,'listings/listing.html', context)

def search(request):

    qs=Listing.objects.all()

    ##description
    if 'keywords' in request.GET:
        keywords=request.GET['keywords']

        if keywords:
            qs=qs.filter(description__icontains=keywords)

    ##city
    # if 'city' in request.GET:
    #     city=request.GET['city']

    #     if keywords:
    #         qs=qs.filter(city__iexact=city)

    ##price
    if 'price' in request.GET:
        price=request.GET['price']

        if keywords:
            qs=qs.filter(price__iexact=price)


    if 'bedrooms' in request.GET:
        bedrooms=request.GET['bedrooms']

        if bedrooms:
            qs=qs.filter(bedrooms__gte=bedrooms)

    context={'state_choices':state_choices,'price_choices':price_choices,'bedroom_choices':bedroom_choices, 'listings':qs
       }


    return render(request,'listings/search.html',context)
    
from django.db import models

# Create your models here.


class employee(models.Model):
    name=models.CharField(max_length=64)
    dept=models.CharField(max_length=20)
    doj=models.DateField()
    sal=models.IntegerField()

    def __str__(self):
        return self.name;

from django.shortcuts import render, redirect
from django.contrib import messages, auth
from django.contrib.auth.models import User
from django.contrib.auth import authenticate

from contacts.models import contacts



def login(request):
    if request.method=='POST':
        username=request.POST['username']
        password=request.POST['password']
        user=authenticate(username=username,password=password)

        if user is not None:   
            auth.login(request,user)
            messages.success(request, "user authentication sucess")
            return redirect('dashboard')
        else:
            messages.error(request, "user authentication failed")

    return render(request,'accounts/login.html')

def register(request):

    if request.method=='POST':

        username=request.POST['username']
        first_name=request.POST['first_name']
        last_name=request.POST['last_name']
        email=request.POST['email']
        password=request.POST['password']
        password2=request.POST['password2']

        ## validations
        if password==password2:
            if User.objects.filter(username=username).exists():
                messages.error(request, "user already exists")
            else:
                if  User.objects.filter(email=email).exists():
                    messages.error(request, "email already exists")
                    return redirect('register')
                else:

                    user=User.objects.create_user(username=username, email=email, password=password, first_name=first_name, last_name=last_name, is_superuser=False, is_staff=True )
                    user.save()
                    messages.success(request,"user registered sucessfully please login")
                    return redirect('login')
        else:
            messages.error(request, "passwords don't match")
            
            
    return render(request,'accounts/register.html')

def logout(request):
    if request.method=="POST":
        auth.logout(request)
        messages.success(request, "successfully logout")
    

    return redirect('index')

def dashboard(request):

    user_contacts=contacts.objects.order_by('-contact_date').filter(user_id=request.user.id)
    context={'contacts':user_contacts}
    return render(request,'accounts/dashboard.html', context)



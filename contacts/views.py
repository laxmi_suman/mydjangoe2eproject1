from django.shortcuts import render, redirect
from .models import contacts
from django.contrib import messages

from django.core.mail import send_mail


# Create your views here.
def contact(request):

    if request.method=='POST':
        listing_id=request.POST['listing_id']
        listing_title=request.POST['listing']
        name=request.POST['name']
        email=request.POST['email']
        phone=request.POST['phone']
        message=request.POST['message']
        user_id=request.POST['user_id']

        #realtor_email=request.POST[]

        #check if user already made an enquiry

        if request.user.is_authenticated:
            user_id=request.user.id
            has_contacted=contacts.objects.all().filter(listing_id=listing_id,user_id=user_id)
            if has_contacted:
                messages.error(request,"your request is already submitted for the property")
            else:
                cont=contacts(listing_id=listing_id,listing_title=listing_title,name=name, email=email,phone=phone,message=message,user_id=user_id )
                cont.save()
                messages.success(request, "contact enquiry submitted successfully realtor will get back to you")

                ##send_mail('Enquiry for property:'+listing_title ,'Hi user please check the enquirer.','lax.sumi11@gmail.com',['sunilrathaur@gmail.com'],fail_silently=False)

            return redirect('/listings/'+listing_id)
            


   # return render(request, 'contact',{})
from django.db import models
from datetime import datetime

# Create your models here.

class contacts(models.Model):
    listing_id=models.IntegerField()
    listing_title=models.CharField(max_length=200)
    name=models.CharField(max_length=200)
    email=models.EmailField()
    phone=models.IntegerField()
    message=models.TextField(blank=True)
    contact_date=models.DateTimeField(default=datetime.now, blank=True)
    user_id=models.IntegerField(blank=True)

    def __str__(self):
        return self.name



